// -----------------------------------------
// Function and Variables with Potentiometers
// -----------------------------------------
// In this example, we register three Particle.variables() with the cloud so that we can read the voltage outputs of three potentiometers.

// We're going to start by declaring which pins everything is plugged into.

int potentiometer = A0; // This is where your potentiometers are plugged in. The other side goes to the "power" pin (below).
int potentiometer2 = A1;
int potentiometer3 = A2;

int power = A5; // This is the pin you are running power from, sending voltage to the three potentiometers.
// That way we can accurately calculate a voltage drop.

int potvalue; // Here we are declaring the variables potvalue, potvalue2, and potvalue3, which we will use later to store the output value of the potentiometers.
int potvalue2;
int potvalue3;

// Next we go into the setup function.

void setup() {

    // First, declare all of our pins. This lets our device know which ones will be used for outputting voltage, and which ones will read incoming voltage.
    pinMode(potentiometer,INPUT);  // Our potentiometer pins are inputs
    pinMode(potentiometer2,INPUT);
    pinMode(potentiometer3,INPUT);
    pinMode(power,OUTPUT); // The pin sending power is output (sending out consistent power)

    // Next, write the power to be the maximum possible, so that we can use this for power.
    digitalWrite(power,HIGH);

    // We are going to declare a Particle.variable() here so that we can access the value of the potentiometers from the cloud.
    Particle.variable("potvalue", &potvalue, INT);
    Particle.variable("potvalue2", &potvalue2, INT);
    Particle.variable("potvalue3", &potvalue3, INT);
    // This is saying that when we ask the cloud for "potvalue", "potvalue2", or "potvalue3" this will reference the corresponding potvalue variable in this app, which is an integer variable.

}


// Finally is the loop function...

void loop() {

    // check to see what the value of the potentiometer output is and store it in the int variable potvalue
    potvalue = analogRead(potentiometer);
    potvalue2 = analogRead(potentiometer2);
    potvalue3 = analogRead(potentiometer3);

}